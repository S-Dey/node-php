<!DOCTYPE html>
<html lang="en">

<title>NodeJS Demo</title>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
</head>

<body>

    <script>
        var socket = io.connect("http://localhost:1337");

        socket.on("new_order", function(data) {
            console.log(data);
        })

    </script>
    
</body>
</html>